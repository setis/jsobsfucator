<?php

include __DIR__ . '/BootStap.php';

function show_banner($link, $img_domain, $img_file, $params) {
    global $redis;
    if ($code = $redis->get('tds:cache:campaign:' . $params)) {
        $code = str_replace("__CLICKLINK__", $link, $code);
        $code = str_replace("__BANNERDOMAIN__", $img_domain, $code);
        $code = str_replace("__BANNERFILE__", $img_file, $code);
    } else {
        $array = array(
            'link' => '__CLICKLINK__',
            'img' => 'http://__BANNERDOMAIN__/__BANNERFILE__'
        );
        $code = \jsObsfucator\Methods\Banner::run($array);
        $redis->set('tds:cache:campaign:' . $params, $code);
        $code = str_replace("__CLICKLINK__", $link, $code);
        $code = str_replace("__BANNERDOMAIN__", $img_domain, $code);
        $code = str_replace("__BANNERFILE__", $img_file, $code);
    }
    header('Content-Type: text/javascript',true);
    echo $code;
}

function show_code($link) {
    header('Content-Type: text/javascript',true);
    echo \jsObsfucator\Methods\Traffic::run($link);
}
 __HALT_COMPILER();