<?php

namespace jsObsfucator\Bases;

use jsObsfucator\iFace\Recovery;

class Limit implements Recovery {

    /**
     *
     * @var limit
     */
    public $limit = array();

    /**
     *
     * @var Array
     */
    public $counter = array();

    /**
     *
     * @var Array
     */
    public $array;

    public function set($i, $limit) {
        if (!is_int($i)) {
            $id = array_search($i, $this->array);
            if ($id === false) {
                return false;
            }
        } else {
            $id = $i;
        }
        $this->limit[$id] = $limit;
        return true;
    }

    public function get($i) {
        if (!is_int($i)) {
            $id = array_search($i, $this->array);
            if ($id === false) {
                return;
            }
        } else {
            $id = $i;
        }
        return $this->limits[$id];
    }

    public function isLimit($i) {
        if (!isset($this->counter[$i])) {
            $this->counter[$i] = 0;
        }

        $result = ($this->limit === null || !isset($this->limit[$i]) || $this->limit[$i] >= $this->counter[$i]) ? false : true;
        if ($result) {
            $this->counter[$i] ++;
        }
        return $result;
    }

    public function reset() {
        $this->counter = 0;
    }

    public function restore($counter) {
        $this->counter = $counter;
    }

    public function save() {
        return $this->counter;
    }

}
