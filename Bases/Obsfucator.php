<?php

namespace jsObsfucator\Bases;

use Exception,
    jsObsfucator\iFace\Recovery,
    jsObsfucator\iFace\Data,
    jsObsfucator\iFace\iSelf,
    jsObsfucator\iFace\Reset;

class Obsfucator {

    /**
     *
     * @var Gc
     */
    public $gc;

    /**
     *
     * @var Limit
     */
    public $limit;

    /**
     *
     * @var objVar
     */
    public $var;

    /**
     *
     * @var objFunc
     */
    public $func;

    /**
     *
     * @var objArgc
     */
    public $argc;

    /**
     *
     * @var objValue
     */
    public $value;

    /**
     *
     * @var Size
     */
    public $size;

    /**
     *
     * @var Array
     */
    public $list_data = array('var', 'func', 'argc', 'value');

    /**
     *
     * @var Array
     */
    public $list_recovery = array('var', 'func', 'argc');

    /**
     *
     * @var string
     */
    public $version = '1.0.1';

    public function __construct() {
        $this->gc = new Gc();
        $this->var = new objVar();
        $this->func = new objFunc();
        $this->argc = new objArgc();
        $this->size = new Size();
        $this->value = new objValue();
        $this->iself();
    }

    public function __clone() {
        $obj = clone $this;
        $obj->reset();
        return $obj;
    }

    public function data($data) {
        if (!is_callable($data)) {
            throw new Exception('not callable');
        }
        foreach ($this->list_data as $var) {
            if ($this->{$var} instanceof Data) {
                $this->{$var}->data = $data;
            }
        }
    }

    public function iself() {
        foreach ($this as $var => &$value) {
            if ($value instanceof iSelf) {
                $value->iself($this);
            }
        }
    }

    public function reset() {
        foreach ($this->list_recovery as $var) {
            if ($this->{$var} instanceof Reset) {
                $this->{$var}->reset();
            }
        }
    }

    public function inject($content, array $nArray = null) {
        $script = explode("\n", $content);
        $count = count($script);
        $garbe_size = $this->size->get('garbare', $count);
        $inject = array();
        while ($count !== count($inject)) {
            $n = rand(1, $garbe_size);
            if ($nArray === null || !in_array($n, $nArray)) {
                $inject[] = $n;
                $inject = array_unique($inject);
            }
        }
        $m = array();
        sort($inject);
        $s = 0;
        foreach ($inject as $n => $i) {
            if ($nArray === null || !in_array($n, $nArray)) {
                if ($s < $i) {
                    $b = $i - $s;
                    while ($b--) {
                        $s++;
                        $m[] = Wrapper::garbare($this->list_recovery, array($this->gc, 'garbare'), $this);
                    }
                }
            }

            $m[] = $script[$n];
            $s++;
        }
        return str_replace(array("\r", "\t", "\n"), '', implode("\n", $m));
    }

    public function argc() {
        $i = $this->size->get('argc');
        $i-=count($this->argc);
        while ($i > 0 && $i--) {
            $this->argc->create(false);
        }
    }

    public function fake_block() {
        $i = $this->size->get('fake.block');
        $block = '';
        while ($i) {
            $block.=$this->garbare();
        }
        return $block;
    }

    /**
     * 
     * @param string $block
     * @param string|null $func
     * @param boolean $call
     * @return string
     */
    public function func($block = null, &$func = null, $call = true) {
        if ($func === null) {
            $func = $this->func->rand();
        }
        $args = $this->argc();
        if ($block === null) {
            $block = $this->content();
        }
        $block = $this->inject($block);
//        var_dump($this->argc->toArray(), $this->argc->list, $this->argc->uniq, 'args');
        $result = Wrapper::func($func, $block, $this->argc->toArray());
        if ($call) {
            $result .=Wrapper::call($func);
        }

        $this->reset();

        return $result;
    }

    public function dump($name = 'dump.json') {
        $result = array();
        foreach ($this->list_recovery as $var) {
            $result[$var] = $this->{$var}->save();
        }
        file_put_contents($name, json_encode($result));
    }

    public function fake($func, $call = true) {
        $i = $this->size->get('fake.func');
        if (is_callable($func)) {
            throw new Exception('not is callable');
        }
        $block = array();
        while ($i--) {
            $block[] = $this->fake_func(call_user_func($func), $call);
        }
        return $block;
    }

    public function content() {
        if (!method_exists($this, 'method')) {
            throw new Exception('not found method  is ' . get_class($this) . '::method');
        }
        return $this->method();
    }

    public function mix($func, array $fake) {
        $fake[] = $func;
        $list = array_keys($fake);
        shuffle($list);
        $result = '';
        foreach ($list as $i) {
            $result.=$fake[$i];
        }
        return $result;
    }

    public function fake_func(&$func = null, $call = true) {
        if ($func === null) {
            $func = $this->func->rand();
        }
        $args = $this->argc();
        $block = $this->fake_block();
        $result = Wrapper::func($func, $block, $args);
        if ($call === true) {
            $result .=Wrapper::call($func);
        }
        $this->reset();
        return $result;
    }

    public function randVar($lock = false, $wrapper = true) {
        $case = $this->size->get('var.rand', $this);
        try {
            if ($case === 0) {
                return $this->var->rand($lock, $wrapper);
            } else {
                return $this->argc->rand($lock, $wrapper);
            }
        } catch (Exception $e) {
            return call_user_func_array(array($this, __FUNCTION__), func_get_args());
        }
    }

    public function selectVar($lock = false, $wrapper = true) {
        $case = $this->size->get('var.rand', $this);
        if ($case === 0) {
            return $this->var->select($lock, $wrapper);
        } else {
            return $this->argc->select($lock, $wrapper);
        }
    }

    public function createVar($lock = false, $wrapper = true) {
        $case = $this->size->get('var.rand', $this);
        if ($case === 0) {
            return $this->var->create($lock, $wrapper);
        } else {
            return $this->argc->create($lock, $wrapper);
        }
    }

    public function deleteVar($var, $lock = null) {
        if ($lock === true) {
            if (($i = array_search($var, $this->var->uniq))) {
                unset($this->var->uniq[$i]);
            } elseif (($i = array_search($var, $this->args->uniq))) {
                unset($this->args->uniq[$i]);
            }
        } elseif ($lock === false) {
            if (($i = array_search($var, $this->var->list))) {
                unset($this->var->list[$i]);
            } elseif (($i = array_search($var, $this->args->list))) {
                unset($this->args->list[$i]);
            }
        } else {
            if (($i = array_search($var, $this->var->uniq))) {
                unset($this->var->uniq[$i]);
            } elseif (($i = array_search($var, $this->args->uniq))) {
                unset($this->args->uniq[$i]);
            } elseif (($i = array_search($var, $this->var->list))) {
                unset($this->var->list[$i]);
            } elseif (($i = array_search($var, $this->args->list))) {
                unset($this->args->list[$i]);
            }
        }
    }

    public function irand($count, $proc) {
        return floor(($count / 100) * $proc);
    }

    public function rand($array,$name) {
        $size = $this->size->get($name, count($array));
        $work = $array;
        while (--$size >= 0) {
            $i = array_rand($work);
            unset($work[$i]);
        }
        return $work;
    }

}
