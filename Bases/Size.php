<?php

namespace jsObsfucator\Bases;

use Exception,
    jsObsfucator\iFace\Recovery;

class Size {

    public $size = array();

    public function set($name, $func, $overwrite = false) {
        if (!is_callable($func)) {
            throw new Exception('not callable name:' . $name);
        }
        if (isset($this->size[$name]) && $overwrite === false) {
            throw new Exception('already exists name:' . $name);
        }
        $this->size[$name] = $func;
    }

    public function get($name, $argument = null) {
        if (!isset($this->size[$name])) {
            throw new Exception('not found name:' . $name);
        }
        return call_user_func($this->size[$name], $argument);
    }

}
