<?php

namespace jsObsfucator\Bases;

use jsObsfucator\iFace\Data,
    jsObsfucator\iFace\iSelf;

class objValue implements Data, iSelf {

    public $data;
    public $wrapper = '\'';
    public static $values = array(
        'undefined',
        'true',
        'false',
        'null'
    );

    /**
     *
     * @var \jsObsfucator\Bases\Obsfucator
     */
    public $self;

    public function normal($wrapper = true) {
        return ($wrapper === false) ? call_user_func($this->data) : Wrapper::value(call_user_func($this->data));
    }

    public function long($wrapper = true, $prefix = ' ') {
        $i = $this->self->size->get('value.long');
        $value = '';
        while ($i--) {
            $value.=call_user_func($this->data) . $prefix;
        }
        return ($wrapper === false) ? $value : Wrapper::value($value);
    }

    public static function standart() {
        return self::$values[array_rand(self::$values)];
    }

    public static $tags = array(
        'img', 'a',
    );

    public function tag() {
        $tag = self::$tags[array_rand(self::$tags)];
        return 'document.createElement(' . $this->wrapper . $tag . $this->wrapper . ')';
    }

    public static $methods = array(
        'normal', 'long', 'standart', 'tag'
    );
    public $methods_work;

    public function rand($wrapper = true) {
        if ($this->methods_work === null) {
            $this->methods_work = $this->self->rand(self::$methods,'value.method');
        }
        $method = $this->methods_work[array_rand($this->methods_work)];
        return $this->{$method}($wrapper);
    }

    public static $typeof = array(
        'undefined',
        'object',
        'boolean',
        'number',
        'string',
        'function'
    );
    public $typeof_work;

    public function typeof() {
        if ($this->typeof_work === null) {
            $this->typeof_work = $this->self->rand(self::$typeof,'value.typeof');
        }
        return $this->typeof_work[array_rand($this->typeof_work)];
    }

    public function iself($self) {
        $this->self = $self;
        return $this;
    }

}
