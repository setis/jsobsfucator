<?php

namespace jsObsfucator\Bases;

use Exception;

abstract class Method extends Obsfucator {

    /**
     * @var Array
     */
    protected $methods;

    public function __construct() {
        parent::__construct();
        $this->methods = $this->methods();
        $this->init();
    }

    public static function minify($source, $modeMinJs = 1) {
        switch ($modeMinJs) {
            case 2:
                $file = tempnam("/tmp");
                file_put_contents($file, $source);
                exec("java -jar compiler.jar --charset UTF-8 --js $file --js_output_file $file.min");
                $js = file_get_contents($file . '.min');
                unlink($file);
                unlink($file . '.min');
                break;
            case 3:
                $file = tempnam("/tmp");
                file_put_contents($file, $source);
                exec("nodejs uglifyjs $file -o $file.min");
                $js = file_get_contents($file . '.min');
                unlink($file);
                unlink($file . '.min');
                break;
            default:
            case 1:
                $js = \JsMin\Minify::minify($source);
                break;
        }
        return $source;
    }

    public function init() {
        $this->size->set('argc', function() {
            return (mt_rand(0, 1)) ? mt_rand(($i = mt_rand(0, 2)), mt_rand($i, 6)) : 0;
        });
        $this->size->set('garbare', function($content) {
            return mt_rand(($content * ($i = round(mt_rand(1, 2)))), ($content * mt_rand($i, 5)));
        });
        $this->data(function() {
            global $redis;
            return trim($redis->sRandMember('tds:word:base'));
        });
//        $this->redis = new \Redis();
//        $this->redis->connect('127.0.0.1');
//        $this->data(function() {
//            return trim($this->redis->sRandMember('var'));
//        });
        $this->size->set('var.rand', function(Obsfucator $self) {
            return mt_rand(0, 1);
        });
    }

    /**
     * 
     * @return Array
     */
    public function methods() {
        return array();
    }

    /**
     * 
     * @return string
     * @throws \Exception
     */
    public function method() {
        switch (count($this->methods)) {
            case 0:
                throw new Exception('not found methods');
            case 1:
                $call = $this->methods[0];
                break;
            default:
                $call = $this->methods[array_rand($this->methods)];
                break;
        }
        return call_user_func(array($this, $call));
    }

    /**
     * 
     * @return this
     */
    public static function instance() {
        $class = get_called_class();
        return new $class();
    }

    /**
     * 
     * @return this
     */
    public static function run(array $array) {
        $class = get_called_class();
        $self = new $class();
        foreach ($array as $var => $value) {
            $self->{$var} = $value;
        }
        return $self->func();
    }

}
