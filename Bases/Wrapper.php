<?php

namespace jsObsfucator\Bases;

use Exception,
    jsObsfucator\iFace\Recovery,
    ReflectionClass,
    ReflectionMethod;

class Wrapper {

    public static function prefix($data, $plode = ' ') {
        $prefix = null;
        $arr = explode($plode, trim($data));
        if (count($arr) === 2) {
            list($prefix, $data) = $arr;
        } else {
            $data = $arr[0];
        }
        return array($data, $prefix);
    }

    public static function garbare(array $list, $garbe, $self, $limit = 3000) {
        $recovery = array();
        foreach ($list as $var) {
            $obj = $self->{$var};
            if ($obj instanceof Recovery) {
                $recovery[$var] = $obj->save();
            } else {
                $recovery[$var] = $self->{$var};
            }
        }
        while (true) {
            $call = call_user_func($garbe);
            try {
                return call_user_func($call, $self);
            } catch (Exception $e) {
                foreach ($recovery as $var => $value) {
                    if ($obj instanceof Recovery) {
                        $self->{$var}->restore($value);
                    } else {
                        $self->{$var} = $value;
                    }
                }
                if ($limit !== null && --$limit < 1) {
                    throw new Exception('limit');
                }
            }
        }
    }

    public static function value($value) {
        return "'$value'";
    }

    public static function set($var, $value) {
        return "$var = $value;";
    }

    public static function block_if($if, $block) {
        return 'if(' . $if . '){' . $block . '}';
    }

    public static function func($func, $block, array $argc = null) {
        if (is_array($argc)) {
            $argc = implode(',', $argc);
        }
        list($func) = self::prefix($func);
        return 'function ' . $func . '(' . $argc . '){' . $block . '}';
    }

    public static function call($func) {
        list($func, ) = self::prefix($func, ' ');
        return "$func();";
    }

    public static function setTimeout($code, $timeout = 750) {
        return "setTimeout(function(){{$code}},{$timeout});";
    }

}
