<?php

namespace jsObsfucator\Bases;

use ArrayAccess,
    Countable,
    Iterator,
    Exception,
    jsObsfucator\iFace\Reset,
    jsObsfucator\iFace\Recovery,
    ReflectionClass,
    ReflectionMethod;

class Gc implements ArrayAccess, Countable, Iterator, Reset, Recovery {

    /**
     *
     * @var Array|callable
     */
    public $gc = array();

    /**
     *
     * @var Limit
     */
    public $limit;

    /**
     *
     * @var int
     */
    private $position = 0;

    public function __construct() {
        $this->limit = new Limit();
        $this->limit->array = &$this->gc;
    }

    public function reset() {
        $this->limit->reset();
    }

    public function count() {
        return count($this->gc);
    }

    public function current() {
        return $this->gc[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function valid() {
        return isset($this->gc[$this->position]);
    }

    public function offsetExists($offset) {
        return isset($this->gc[$offset]);
    }

    public function offsetGet($offset) {
        return isset($this->gc[$offset]) ? $this->gc[$offset] : null;
    }

    public function offsetSet($offset, $value) {
        if ($offset == null) {
            $this->gc[] = $value;
        } else {
            $this->gc[$offset] = $value;
        }
    }

    public function offsetUnset($offset) {
        unset($this->gc[$offset]);
    }

    public static function methods($self) {
        $class = new ReflectionClass($self);
//        $methodsStatic = $class->getMethods(ReflectionMethod::IS_STATIC);
//        $methodsPublic = $class->getMethods(ReflectionMethod::IS_PUBLIC);
//        $methods = array_diff($methodsStatic, $methodsPublic);
        $methods = $class->getMethods(ReflectionMethod::IS_STATIC);
        array_walk($methods, function(ReflectionMethod &$value) {
            $value = array($value->class, $value->name);
        });
        return $methods;
    }
    
    public function rand(){
        $count = count($this->gc) - mt_rand(mt_rand(4,8), count($this->gc));
//        var_dump($count);
        for($i = 0;$i<$count;$i++){
            $c = array_rand($this->gc);
            unset($this->gc[$c]);
        }
        $this->gc = array_values($this->gc);
//        var_dump($this->gc);
    }

    public function load($methods) {
        if (is_string($methods)) {
            $methods = self::methods($methods);
        } elseif (is_array($methods)) {
            $methods = array_filter($methods, 'is_callable');
        } else {
            throw new Exception('not input gc->methods ' . __METHOD__);
        }
        if (count($this->gc) === 0) {
            $this->gc = $methods;
            return array_keys($this->gc);
        }
        foreach ($methods as $i => $method) {
            if (in_array($method, $this->gc)) {
                throw new Exception('not uniq ' . $i);
            }
            $result[] = array_push($this->gc, $method) - 1;
        }
        return $result;
    }

    public function add($func) {
        if (!is_callable($func)) {
            throw new Exception('not is callable');
        }
        if (in_array($func, $this->gc)) {
            throw new Exception('not uniq');
        }
        return array_push($this->gc, $func) - 1;
    }

    public function garbare() {
        while (true) {
            $i = array_rand($this->gc);
            if (!$this->limit->isLimit($i)) {
                break;
            }
        }
        return $this->gc[$i];
    }

    public function restore($counter) {
        $this->limit->restore($counter);
    }

    public function save() {
        return $this->limit->save();
    }

}
