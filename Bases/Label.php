<?php

namespace jsObsfucator\Bases;

use Exception;

class Label {

    public $labels = array();
    public $multi = false;

    public function set($label, $data, $throw = true) {
        if ($this->multi === false) {
            if (isset($this->labels[$label]) && $throw === true) {
                throw new Exception('already exists ' . $label);
            }
            $this->labels[$label] = $data;
        } else {
            if (isset($this->labels[$label]) && !($id = array_search($data, $this->labels[$label])) && $throw === true) {
                throw new Exception('already exists ' . $label);
            }
            if ($id === false) {
                $this->labels[$label][] = $data;
            }
        }
    }

    public function get($label, $throw = false) {
        if ($this->multi === false) {
            if (!isset($this->labels[$label]) && $throw === true) {
                throw new Exception('not found ' . $label);
            }
            return $this->labels[$label];
        } else {
            if (!isset($this->labels[$label]) && $throw === true) {
                throw new Exception('not found ' . $label);
            }
        }
    }

}
