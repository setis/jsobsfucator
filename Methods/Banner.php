<?php

namespace jsObsfucator\Methods;

use jsObsfucator\Bases\Method,
    jsObsfucator\Bases\Wrapper;

class Banner extends Method {

    public function method1() {
        list($d, $var_d) = Wrapper::prefix($this->randVar(true));
        list($link, $var_link) = Wrapper::prefix($this->randVar(true));
        list($img, $var_img) = Wrapper::prefix($this->randVar(true));
        list($div, $var_div) = Wrapper::prefix($this->randVar(true));
        return "$var_link $link = document.createElement('a');\n$link.href = '$this->link';\n$var_d $d= document.body;\n$link.target='_blank';\n$var_img $img = document.createElement('img');\n$img.src = '$this->img';\n$link.appendChild($img);\n"
                . "$var_div $div = document.createElement('div');\n$div.appendChild($link);\ndocument.write($div.innerHTML);";
    }

    public function method2() {
        list($body, $var_body) = Wrapper::prefix($this->randVar(true));
        return "document.write('<html><head></<head><body></body></html>');\n"
                . "$var_body $body = document.getElementsByTagName('body')[0];\n"
                . "$body.innerHtml = \"<a href='$this->link'><img src='$this->img'></a>\";\n"
                . "document.write($body.innerHtml);\n";
    }

    public function method3() {
        list($body, $var_body) = Wrapper::prefix($this->randVar(true));
        list($link, $var_link) = Wrapper::prefix($this->randVar(true));
        list($img, $var_img) = Wrapper::prefix($this->randVar(true));
        return "document.write('<html><head></<head><body></body></html>');\n"
                . "$var_link $link = '$this->link';\n"
                . "$var_img $img = '$this->img';\n"
                . "$var_body $body = document.getElementsByTagName('body')[0];\n"
                . "$body.innerHtml = \"<a href='\" + $link + \"'><img src='\" + $img + \"'></a>\";\n"
                . "document.write($body.innerHtml);\n";
    }

    public function method4() {
        $func = $this->func->rand(true, false);
        $result =  $this->func("window.location.href = '$this->link';\n", $func, false);
        $this->func->uniq[] = $func;
        return "document.write(\"<html><head></head><body><img src='$this->img' onclick='$func();'><script>$result<\/script></body></html>\");\n";
    }

    public function methods() {
        return array(
            'method1',
            'method2',
            'method3',
            'method4',
        );
    }

    public function init() {
        $this->size->set('argc', function() {
            return (mt_rand(0, 1)) ? mt_rand(($i = mt_rand(0, 2)), mt_rand($i, 6)) : 0;
        });
        $this->size->set('var.rand', function() {
            return (mt_rand(0, 1));
        });
        $this->size->set('garbare', function($content) {
            return mt_rand(($content * ($i = round(mt_rand($d = mt_rand(1, 5), mt_rand($d, mt_rand($d, 20)))))), ($content * mt_rand($i, mt_rand($i, $i*2))));
        });
        $this->size->set('value.method', function($count) {
            return (mt_rand(0,1))?mt_rand(0,$count-1):0;
        });
        $this->size->set('value.typeof', function($count) {
            return (mt_rand(0,1))?mt_rand(0,$count-1):0;
        });
        $this->size->set('value.long', function() {
            return mt_rand(4,  rand(5, 24));
        });
        /**
          $this->redis = new \Redis();
          $this->redis->connect('127.0.0.1');
          $this->data(function() {
          return trim($this->redis->sRandMember('var'));
          });
         */
        $this->data(function() {
            global $redis;
            $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
            if ($result === null) {
                exit('not found records is redis');
            }
            return $result;
        });
        $this->gc->load('jsObsfucator\Gc');
        $this->gc->rand();
    }

    public static function show($link, $img) {
        self::run(array('link' => $link, 'img' => $img));
    }

}
