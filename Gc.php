<?php

namespace jsObsfucator;

use jsObsfucator\Bases\Obsfucator,
    jsObsfucator\Bases\Wrapper,
    Exception;

class Gc {

    public static function garbe_1(Obsfucator $self) {
        $i = $self->var->select();
        $d = $self->randVar(false);
        $s = (rand(0, 1)) ? $self->var->select() : $self->value->rand();
        if ($i === $d) {
            throw new Exception();
        }
        return "if('undefined'!=typeof $i){ $d = $s;}";
    }

    public static function garbe_2(Obsfucator $self) {
        $i = $self->randVar();
        $j = $self->value->long();
        return "$i=$j.split(' ');";
    }

    public static function garbe_3(Obsfucator $self) {
        $create = $self->randVar(false);
        $select = $self->selectVar(false);
        list($check) = Wrapper::prefix($create);
        if ($check === $select) {
            throw new Exception();
        }
        return $create . "=$select;";
    }

    public static function garbe_4(Obsfucator $self) {
        return Wrapper::set($self->randVar(), $self->value->rand());
    }

    public static function garbe_5(Obsfucator $self) {
        $keyValue = $self->selectVar(false);
        $list = $self->selectVar(false, false);
        $i = $self->createVar(false, false);
        $result = " if($keyValue instanceof Object){var $list = [];"
                . "for(var $i in $keyValue){ $list.push(" . $keyValue . "[$i]);}}";
        return $result;
    }

    public static function garbe_6(Obsfucator $self) {
        $arr = $self->selectVar(false);
        $callback = $self->selectVar(false, false);
        $key = $self->createVar(false, false);
        $result = "if($arr instanceof Array){if($arr.forEach !== undefined && typeof $arr.forEach === 'function'){if(typeof $callback === 'function' ){ $arr.forEach($callback); }}else{if($callback instanceof Object){ for(var $key in $arr){ $callback(" . $arr . "[$key],$key,$arr);}}}}";
//        $self->unlock($arr, $key, $callback);
        return $result;
    }

    public static function garbe_7(Obsfucator $self) {
        $obj = $self->selectVar(false);
        $parts = $self->createVar(false, false);
        $key = $self->createVar(false, false);
        return " if(typeof $obj === 'object' || $obj instanceof Array){var $parts = [];for(var $key in $obj){ $parts.push($key);}}";
    }

    public static function garbe_8(Obsfucator $self) {
        $i = mt_rand(2, mt_rand(5, 10));
        while ($i--) {
            $result[] = $self->var->create(false, false);
        }
        return ' var ' . implode(',', $result) . ';';
    }

    public static function garbe_9(Obsfucator $self) {
        static $list = array(
            '[]', '{}', 'new Array()'
        );
        return Wrapper::set($self->randVar(), $list[mt_rand(0, 2)]);
    }

    public static function garbe_10(Obsfucator $self) {
        static $list = array(
            '+=', '-='
        );
        return $self->selectVar().$list[mt_rand(0, 1)].$self->selectVar().';';
    }
    public static function garbe_11(Obsfucator $self) {
        static $list = array(
            '+=', '-='
        );
        return $self->selectVar().$list[mt_rand(0, 1)].  mt_rand(2, mt_rand(3,5000)).';';
    }
    public static function garbe_12(Obsfucator $self) {
        static $list = array(
            '--', '++'
        );
        return $self->selectVar().$list[mt_rand(0, 1)].';';
    }
    
    public static function garbe_13(Obsfucator $self) {
        static $list = array(
            '==', '!=','===','!=='
        );
        $s = rand(1,5);
        $gc='';
        while($s--){
            $gc.=Wrapper::garbare($self->list_recovery, array($self->gc, 'garbare'), $self);
        }
        return Wrapper::block_if($self->selectVar().$list[mt_rand(0, 3)].$self->selectVar(),$gc);
    }
    public static function garbe_14(Obsfucator $self) {
        $obj = array();
        $i = rand(1, mt_rand(2,10));
        while ($i--){
            $obj[$self->value->normal(false)] = (mt_rand(0, 1))?$self->value->rand():$self->selectVar();
        }
        foreach($obj as $var=>$value){
            $result[]= "$var:$value";
        }
        return Wrapper::set($self->var->rand(), '{'.implode(',', $result).'}');
    }
/**    
    public static function garde_15(Obsfucator $self){
        $vars = '['.implode(',', $self->var->list).'];';
        $count = count($self->var->list);
        $var = $self->createVar(false,false);
        $i = $self->createVar(false,false);
        $value = $self->createVar(false,false);
        $result = "var $var = $vars;".
                "for(var $i in $var){".
                "var $value = $var"."[$i];".
                "if(".$self->value->typeof()."!== typeof $value){".
                "delete($value);".
                "}}";
        return $result;
    }
    public static function garde_16(Obsfucator $self){
        $vars = '['.implode(',', $self->var->list).'];';
        
        $count = count($self->var->list);
        $var = $self->createVar(false,false);
        $i = $self->createVar(false,false);
        $value = $self->createVar(false,false);
        $result = "var $var = $vars;".
                "for(var $i in $var){".
                "var $value = $var"."[$i];".
                "if(".$self->value->typeof()."!== typeof $value){".
                "delete($value);".
                "}}";
        return $result;
    }
**/
}
