<?php

namespace jsObsfucator;

class BootStrap {

    public static function register() {
        spl_autoload_register(array(__CLASS__, 'autoload'), false, true);
        set_include_path(get_include_path() . PATH_SEPARATOR . __DIR__);
    }

    public static function unregister() {
        spl_autoload_unregister(array(__CLASS__, 'autoload'));
    }

    public static function autoload($name) {
        $arr = array_filter(explode('\\', $name), function($value) {
            return (!empty($value));
        });
        $namespace = ltrim(array_shift($arr));
        if (__NAMESPACE__ !== $namespace) {
            return false;
        } else {
            $file = __DIR__ . '/' . implode('/', $arr) . '.php';
            if (!file_exists($file)) {
                return false;
            }
            include $file;
        }
        return true;
    }

}

\jsObsfucator\BootStrap::register();
