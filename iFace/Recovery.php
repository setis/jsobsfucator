<?php

namespace jsObsfucator\iFace;

interface Recovery {

    public function save();

    public function restore($array);
}
