<?php

namespace jsObsfucator\iFace;

interface Reset {

    public function reset();
}
